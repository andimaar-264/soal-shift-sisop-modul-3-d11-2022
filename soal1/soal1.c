#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<dirent.h>
#include <stdint.h>

pthread_t tid[3];

void *download(char *url, char *locdir) {
    char *argv[] = {"wget", "-q", "--no-check-certificate", url, "-O", locdir, NULL};
    execv("/bin/wget", argv);
}

void *unzip(char *zipdir, char *locdir) {
    char *argv[] = {"unzip", zipdir, "-d", locdir, NULL};
    execv("/bin/unzip", argv);
}

void *unzipPass(char *pass, char *zipdir, char *locdir) {
    char *argv[] = {"unzip", "-P", pass, zipdir, NULL};
    execv("/bin/unzip", argv);
}

void *makeZip(char *pass, char *zipName, char *locdir) {
    char *argv[] = {"zip", "-P", pass, "-r", zipName, locdir, NULL};
    execv("/bin/zip", argv);
}

void *makeZip2(char *pass, char *zipName, char *locdir, char *locdir1) {
    char *argv[] = {"zip", "-P", pass, zipName, locdir, locdir1, NULL};
    execv("/bin/zip", argv);
}

void *makeDir(char *locdir) {
    char *argv[] = {"mkdir", "-p", locdir, NULL};
    execv("/bin/mkdir", argv);
}

void moveDir(char *locdir, char *locdir1) {
    char *argv[] = {"mv", locdir, locdir1, NULL};
    execv("/bin/mv", argv);
}

#define WHITESPACE 64
#define EQUALS 65
#define INVALID 66

static const unsigned char d[] = {
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 64, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 62, 66, 66, 66, 63, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 66, 66, 66, 65, 66, 66, 66, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 66, 66, 66, 66, 66, 66, 26, 27, 28,
    29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66, 66,
    66, 66, 66, 66, 66, 66};

int base64decode(char *in, size_t inLen, unsigned char *out, size_t *outLen)
{
    char *end = in + inLen;
    char iter = 0;
    uint32_t buf = 0;
    size_t len = 0;

    while (in < end)
    {
        unsigned char c = d[*in++];

        switch (c)
        {
        case WHITESPACE:
            continue;
        case INVALID:
            return 1;
        case EQUALS:
            in = end;
            continue;
        default:
            buf = buf << 6 | c;
            iter++;

            if (iter == 4)
            {
                if ((len += 3) > *outLen)
                    return 1;
                *(out++) = (buf >> 16) & 255;
                *(out++) = (buf >> 8) & 255;
                *(out++) = buf & 255;
                buf = 0;
                iter = 0;
            }
        }
    }

    if (iter == 3)
    {
        if ((len += 2) > *outLen)
            return 1;
        *(out++) = (buf >> 10) & 255;
        *(out++) = (buf >> 2) & 255;
    }
    else if (iter == 2)
    {
        if (++len > *outLen)
            return 1;
        *(out++) = (buf >> 4) & 255;
    }

    *outLen = len;
    return 0;
}

void *appendFile(char *locdir, char *decoded) {
    FILE *fp;
    fp = fopen(locdir, "a");
    fprintf(fp, "%s\n", decoded);
    fclose(fp);
}

void *poinA() {
    pid_t child_id;
    int status;
    child_id = fork();

    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            download("drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "music.zip");
        }else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                unzip("music.zip", "./music");
            }
        }
    } else if(pthread_equal(id, tid[1])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            download("drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "quote.zip");
        }else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                unzip("quote.zip", "./quote");
            }
        }
    }
}

void *poinB() {
    pid_t child_id;
    int status;
    child_id = fork();
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if(child_id == 0) {
            struct dirent **namelist;
            int n;

            n = scandir("/home/wicaksono/SISOP/Modul3/music/", &namelist, NULL, alphasort);
            for(int i = 0; i < n; i++) {
                if(strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0) {
                    char locdir[101] = "/home/wicaksono/SISOP/Modul3/music/";
                    strcat(locdir, namelist[i]->d_name);
                    FILE *fp = fopen(locdir, "r");
                    char line[101];
                    char *out;
                    size_t out_len;

                    while (fgets(line, 100, fp) != NULL) {
                        out_len = strlen(line);
                        out = malloc(sizeof(char) * 73728);

                        int err = base64decode(line, out_len, out, &out_len);
                        if(err)
                            printf("Error: %d\n", err);
                    }
                    printf("%s\n", out);
                    fclose(fp);
                    appendFile("/home/wicaksono/SISOP/Modul3/music/music.txt", out);
                    free(namelist[i]);
                }
            }
            free(namelist);
        }
    } else if(pthread_equal(id, tid[1])) {
       if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if(child_id == 0) {
            struct dirent **namelist;
            int n;

            n = scandir("/home/wicaksono/SISOP/Modul3/quote/", &namelist, NULL, alphasort);
            for(int i = 0; i < n; i++) {
                if(strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0) {
                    char locdir[101] = "/home/wicaksono/SISOP/Modul3/quote/";
                    strcat(locdir, namelist[i]->d_name);
                    FILE *fp = fopen(locdir, "r");
                    char line[101];
                    char *out;
                    size_t out_len;

                    while (fgets(line, 100, fp) != NULL) {
                        out_len = strlen(line);
                        out = malloc(sizeof(char) * 73728);

                        int err = base64decode(line, out_len, out, &out_len);
                        if(err)
                            printf("Error: %d\n", err);
                    }
                    printf("%s\n", out);
                    fclose(fp);
                    appendFile("/home/wicaksono/SISOP/Modul3/quote/quote.txt", out);
                    free(namelist[i]);
                }
            }
            free(namelist);
        }
    }
}

void *poinC() {
    pid_t child_id;
    int status;
    child_id = fork();
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            makeDir("/home/wicaksono/SISOP/Modul3/hasil/");
        } else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                moveDir("/home/wicaksono/SISOP/Modul3/music/music.txt", "/home/wicaksono/SISOP/Modul3/hasil/");
            }
        }
    } else if(pthread_equal(id, tid[1])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            sleep(3);
            moveDir("/home/wicaksono/SISOP/Modul3/quote/quote.txt", "/home/wicaksono/SISOP/Modul3/hasil/");
        }
    }
}

void *poinD() {
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0){
        makeZip("mihinomenestwicaksono", "hasil.zip", "./hasil/");
    }
}

void *poinE() {
    pid_t child_id;
    int status;
    child_id = fork();
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            unzipPass("mihinomenestwicaksono", "hasil.zip", "./");
        } else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                sleep(5);
                makeZip2("mihinomenestwicaksono", "hasil.zip", "./hasil/", "no.txt");
            }
        }
    } else if(pthread_equal(id, tid[1])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            appendFile("/home/wicaksono/SISOP/Modul3/no.txt", "NO");
        }
    }
}

int main(void) {
    int  thread1, thread2;

    thread1 = pthread_create(&tid[0], NULL, poinA, NULL);
    if(thread1) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread1);
        exit(EXIT_FAILURE);
    }

    thread2 = pthread_create(&tid[1], NULL, poinA, NULL);
    if(thread2) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread2);
        exit(EXIT_FAILURE);
    }

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    sleep(5);

    thread1 = pthread_create(&tid[0], NULL, poinB, NULL);
    if(thread1) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread1);
        exit(EXIT_FAILURE);
    }

    thread2 = pthread_create(&tid[1], NULL, poinB, NULL);
    if(thread2) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread2);
        exit(EXIT_FAILURE);
    }

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    sleep(5);

    thread1 = pthread_create(&tid[0], NULL, poinC, NULL);
    if(thread1) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread1);
        exit(EXIT_FAILURE);
    }

    thread2 = pthread_create(&tid[1], NULL, poinC, NULL);
    if(thread2) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread2);
        exit(EXIT_FAILURE);
    }

    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    sleep(5);

    thread1 = pthread_create(&tid[0], NULL, poinD, NULL);
    if(thread1) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread1);
        exit(EXIT_FAILURE);
    }
    pthread_join(tid[0], NULL);
    sleep(5);

    thread1 = pthread_create(&tid[0], NULL, poinE, NULL);
    if(thread1) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread1);
        exit(EXIT_FAILURE);
    }

    thread2 = pthread_create(&tid[1], NULL, poinE, NULL);
    if(thread2) {
        fprintf(stderr,"Error - pthread_create() return code: %d\n", thread2);
        exit(EXIT_FAILURE);
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    sleep(5);
    exit(0);
    return 0;
}
