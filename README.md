# soal shift sisop modul 3 d11 2022

Soal Shift Sisop Modul 3 D11 2022

Benedictus Bimo Cahyo Wicaksono - 5025201097<br>
Hasna Lathifah Purbaningdyah - 5025201108<br>
Muhammad Andi Akbar Ramadhan - 5025201264<br>

## Soal 1

#### Poin A
Melakukan download 2 file `.zip` dari Google Drive dan unzip kedua file `.zip` ke dua folder berbeda.
```
void *poinA() {
    pid_t child_id;
    int status;
    child_id = fork();

    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            download("drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", "music.zip");
        }else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                unzip("music.zip", "./music");
            }
        }
    } else if(pthread_equal(id, tid[1])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            download("drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", "quote.zip");
        }else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                unzip("quote.zip", "./quote");
            }
        }
    }
}
```

#### Poin B
Untuk setiap file `.txt` di dalam kedua folder hasil unzip, dilakukan decode dan append hasil decode kedalam file `.txt` baru. Agar hasil decode urut sesuai dengan file dari `.zip`, digunakan `scandir()` dengan parameter `alphasort`.
```
void *poinB() {
    pid_t child_id;
    int status;
    child_id = fork();
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if(child_id == 0) {
            struct dirent **namelist;
            int n;

            n = scandir("/home/wicaksono/SISOP/Modul3/music/", &namelist, NULL, alphasort);
            for(int i = 0; i < n; i++) {
                if(strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0) {
                    char locdir[101] = "/home/wicaksono/SISOP/Modul3/music/";
                    strcat(locdir, namelist[i]->d_name);
                    FILE *fp = fopen(locdir, "r");
                    char line[101];
                    char *out;
                    size_t out_len;

                    while (fgets(line, 100, fp) != NULL) {
                        out_len = strlen(line);
                        out = malloc(sizeof(char) * 73728);

                        int err = base64decode(line, out_len, out, &out_len);
                        if(err)
                            printf("Error: %d\n", err);
                    }
                    printf("%s\n", out);
                    fclose(fp);
                    appendFile("/home/wicaksono/SISOP/Modul3/music/music.txt", out);
                    free(namelist[i]);
                }
            }
            free(namelist);
        }
    } else if(pthread_equal(id, tid[1])) {
       if (child_id < 0) {
            exit(EXIT_FAILURE);
        }
        if(child_id == 0) {
            struct dirent **namelist;
            int n;

            n = scandir("/home/wicaksono/SISOP/Modul3/quote/", &namelist, NULL, alphasort);
            for(int i = 0; i < n; i++) {
                if(strcmp(namelist[i]->d_name, ".") != 0 && strcmp(namelist[i]->d_name, "..") != 0) {
                    char locdir[101] = "/home/wicaksono/SISOP/Modul3/quote/";
                    strcat(locdir, namelist[i]->d_name);
                    FILE *fp = fopen(locdir, "r");
                    char line[101];
                    char *out;
                    size_t out_len;

                    while (fgets(line, 100, fp) != NULL) {
                        out_len = strlen(line);
                        out = malloc(sizeof(char) * 73728);

                        int err = base64decode(line, out_len, out, &out_len);
                        if(err)
                            printf("Error: %d\n", err);
                    }
                    printf("%s\n", out);
                    fclose(fp);
                    appendFile("/home/wicaksono/SISOP/Modul3/quote/quote.txt", out);
                    free(namelist[i]);
                }
            }
            free(namelist);
        }
    }
}
```

#### Poin C
File `.txt` yang berisi hasil decode dari setiap file dipindahkan dengan fungsi `moveDir()`. Sebelum melakukan perpindahan file, dilakukan terlebih dahulu pembuatan folder baru yang sesuai dengan path yang diminta.
```
void *poinC() {
    pid_t child_id;
    int status;
    child_id = fork();
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            makeDir("/home/wicaksono/SISOP/Modul3/hasil/");
        } else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                moveDir("/home/wicaksono/SISOP/Modul3/music/music.txt", "/home/wicaksono/SISOP/Modul3/hasil/");
            }
        }
    } else if(pthread_equal(id, tid[1])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            sleep(3);
            moveDir("/home/wicaksono/SISOP/Modul3/quote/quote.txt", "/home/wicaksono/SISOP/Modul3/hasil/");
        }
    }
}
```

#### Poin D
Pada poin D, diminta untuk membuat file `.zip` dari folder hasil dengan password yang telah ditentukan. Sehingga di sini hanya digunakan 1 thread saja.
```
void *poinD() {
    pid_t child_id;
    int status;
    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if(child_id == 0){
        makeZip("mihinomenestwicaksono", "hasil.zip", "./hasil/");
    }
}
```

Untuk pembuatan zipnya, digunakan argumen `-P` yang kemudian diikuti oleh password yang akan digunakan oleh file `.zip` nantinya.
```
void *makeZip(char *pass, char *zipName, char *locdir) {
    char *argv[] = {"zip", "-P", pass, "-r", zipName, locdir, NULL};
    execv("/bin/zip", argv);
}
```

#### Poin E
Pada poin E, digunakan kembali 2 thread seperti sebelumnya. Di sini, kedua thread tidak melakukan pekerjaan dengan waktu selesai yang bersamaan. Thread pertama akan melakukan unzip file `.zip` yang terproteksi oleh password, sedangkan thread kedua akan melakukan append file `NO.txt`. Setelah itu, thread kedua akan berhenti dan thread pertama akan melakukan pembuatan zip kembali seperti pada poin
```
void *poinE() {
    pid_t child_id;
    int status;
    child_id = fork();
    pthread_t id = pthread_self();
    if(pthread_equal(id, tid[0])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            unzipPass("mihinomenestwicaksono", "hasil.zip", "./");
        } else {
            while ((wait(&status)) > 0);
            pid_t child_id;
            int status;
            child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE);
            }

            if (child_id == 0) {
                sleep(5);
                makeZip2("mihinomenestwicaksono", "hasil.zip", "./hasil/", "no.txt");
            }
        }
    } else if(pthread_equal(id, tid[1])) {
        if (child_id < 0) {
            exit(EXIT_FAILURE);
        }

        if(child_id == 0){
            appendFile("/home/wicaksono/SISOP/Modul3/no.txt", "NO");
        }
    }
}
```


#### Demo Program
![Program ketika dijalankan](images/No1-1.png)<br>
*File `.c` ketika dijalankan*

![Hasil akhir dari program](images/No1-2.png)<br>
*Hasil akhir dari program*

![Isi `file.zip` pada hasil akhir](images/No1-3.png)<br>
*Isi `file.zip` pada hasil akhir*

#### Kendala Pengerjaan

Kendala yang saya alami pada saat pengerjaan soal no 1 ini adalah tidak lengkapnya hasil decode pada salah satu file `.txt` di folder `quote`. Namun hal tersebut dapat saya atasi dengan menetapkan `malloc(sizeof(char) * 73728)` sebagai ukuran dari `out`.

## Soal 2

Poin A

Register
```
void regist(char id[], char password[]){
    FILE *fp = fopen("user.txt", "a"); 
    fprintf(fp, "%s:%s\n", id, password);
    fclose(fp);
}
```

Login
```
int login(char id[], char password[])
{
    FILE *fp = fopen("user.txt", "r");
    int is_auth = 0;
    char buffer[1024];
    while (fgets(buffer, 1024, fp) != NULL && is_auth == 0)
    {
        char file_id[1024], file_password[1024];
        char *token = strtok(buffer, ":");
        strcpy(file_id, token);
        token = strtok(NULL, "\n");
        strcpy(file_password, token);

        if (strcmp(id, file_id) == 0 && strcmp(password, file_password) == 0)
        {
            is_auth = 1;
        }
        else
        {
            is_auth = 0;
        }
    }
    fclose(fp);
    return is_auth;
}
```

Poin B + C
tsv
```
int addtsv(){
    FILE *fp;
    fp = fopen("problems.tsv", "a+"); 
    if (fp == NULL) // doesn't exist ~> make file
    {
        printf("File cannot be opened");
        return 1;
    }
    fprintf(fp, "\n%s\t%s", user_data.file, user_data.name);
    fclose(fp);
    return 0;
}
```

Poin D
See
```
else if (strcmp(cmd, "see") == 0)
        { 
            //seeallfile();
        }
```  

Poin E
Download
```
else if (strcmp(cmd, "download") == 0)
        { 
            // setfile(cmd);
            // download(user_data.socket, user_data.file);
        }
```

Poin F
Submit
```
char *cmdcpy, *subimt;
        cmdcpy = cmdt;
        subimt = strstr(cmdcpy, "submit");
        else if(subimt)
        {
		    submitfile(...);
        }
```

Poin G

```
else if (user_data.is_auth == 1){ 
            loginsuccess();
```

## soal 3

**soal3.c**

Fungsi main
```
int main(int argc, char *argv[]) {

	list_file(loc);

	return 0;
}
```
Variabel untuk menyimpan lokasi folder shift3/hartakarun
```
char *loc = "/home/hasna/shift3/hartakarun";
```

Fungsi untuk listing file secara rekursif
```
void list_file(char *locpath)
{
	char path[1000];
	struct dirent *dp;
	struct  stat statbuf;
	pthread_t t_id[200];
	int i=0;
	DIR *dir = opendir(locpath);

	if(!dir)
		return;

	while((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0)
		{

			strcpy(path, locpath);
			strcat(path, "/");
			strcat(path, dp->d_name);

			list_file(path);
			if (isDirectory(path))
			{
			rmdir(path);
			}
			else {
			pthread_create(&t_id[i], NULL, &folder, (void *)path);
			pthread_join(t_id[i], NULL);
			i++;
			}
		}
	}
	closedir(dir);
}
```
Fungsi untuk mengecek suatu file termasuk directory atau bukan
```
int isDirectory(const char*path) {
	struct stat statbuf;
	if(stat(path, &statbuf) != 0)
		return 0;
	return S_ISDIR(statbuf.st_mode);
}
```

Program untuk membuat thread sebanyak jumlah file terdapat di dalam fungsi `list_file`
```
...
else {
			pthread_create(&t_id[i], NULL, &folder, (void *)path);
			pthread_join(t_id[i], NULL);
			i++;
}
...
```

Fungsi untuk memindahkan file ke dalam folder yang sesuai
```
void *folder(void *args) {
	char newpath[1000];
	char *path = (char*)malloc(sizeof(char));
	path = (char*)args;
	char *name = basename(path);
	char *ext =strdup(folder_name(name));
	
	lowercase(ext);
	if(strcmp(ext,"gz")== 0) {
		ext = "tar.gz";
	}
	strcpy(newpath, loc);
	strcat(newpath, "/");
	strcat(newpath, ext);
	strcat(newpath, "/");
	make_dir(newpath);
	strcat(newpath, name);
	rename(path, newpath);
}
```

Fungsi untuk memberi nama folder
```
char *folder_name(char *name) {
	struct stat st;
	char *dot = strrchr(name, '.');
	if (is_hidden(name)) return "Hidden";
	if(!dot || dot == name) return "Unknown";
	else return dot+1;
}
```

Fungsi untuk mengecek apakah suatu file adalah file hidden atau bukan
```
bool is_hidden(const char *name)
{
	if (name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0){
		return true;
	}
	else return false;
}
```

Fungsi untuk membuat nama folder menjadi lowercase
```
void lowercase(char *s) {
	int i=0;
	while(s[i]!='\0') {
		if(s[i]>='A' && s[i]<='Z') {
			s[i]=s[i]+32;
		}
		i++;
	}
}
```

Fungsi untuk membuat directory apabila belum tersedia
```
static void make_dir(const char *dir) {
	if(mkdir(dir,0755) < 0) {
		if (errno != EEXIST) {
			perror(dir);
			exit(1);
		}
	}
}
```

**client.c**
Fungsi main
```

int main (int argc, char const *argv[]) {
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char command[100];
	pid_t child_id;
	FILE *fp;

	scanf("%[^\n]s", command);
	char buffer[1024] = {0};
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons (PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	}
	child_id = fork();
	if(child_id < 0)
 	{
		exit(EXIT_FAILURE);
	}

	if(child_id == 0)
	{
		char *argv[] = {"zip", "-r", "/home/hasna/Client/hartakarun.zip", "/home/hasna/shift3/hartakarun", NULL};
		execv("/bin/zip", argv);
	}
	else {
	while(wait(NULL)>0);
	if(strcmp(command, "send hartakarun.zip") == 0) {
		send(sock, command, strlen(command), 0);
		fp = fopen("hartakarun.zip", "r");
		if(fp == NULL) {
			perror("Error in reading file");
			exit(-1);
		}
		send_file(fp,sock);
		printf("File data sent successfully \n");
	}
	valread = read(sock, buffer,1024);
	}
	printf("Closing the connection\n");
	close(sock);
	return 0;
}
```

Variabel untuk menyimpan lokasi /shift/hartakarun
```
char *loc = "/home/hasna/shift3/hartakarun";
```

Fungsi untuk mengirim file
```
void send_file(FILE *fp, int sockfd) {
	int n;
	char data[1024] = {0};

	while(fgets(data, 1024, fp) != NULL) {
		if(send(sockfd, data, sizeof(data),0) == -1) {
			perror("Error in sending file");
			exit(-1);
		}
		bzero(data,1024);
	}
}
```

**server.c**
```
int main(int argc, char const *argv[]) {
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char buffer[1024] = {0};
	char *message = "hartakarun.zip has been sent";

	if((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT );

	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	if(listen(server_fd, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
		perror("accept");
		exit(EXIT_FAILURE);
	}

	valread = read(new_socket, buffer, 1024);
	printf("%s\n", buffer);
	if(strcmp(buffer, "send hartakarun.zip")==0) {
		send(new_socket, message, strlen(message), 0);
		printf("hartakarun.zip has been sent\n");
	}
	return 0;
}
```
#### Dokumentasi program

![Isi dari folder/shift3/hartakarun](images/image.png) <br>
*Isi dari folder/shift3/hartakarun*

![Isi dari folder Client](images/image1.png) <br>
*Isi dari folder Client*

![Isi dari folder Server](images/image2.png) <br>
*Isi dari folder Server*

#### Kendala Pengerjaan

Kendala pengerjaan pada saat praktikum adalah file `hartakarun.zip` tidak berhasil terkirim ke folder client, tetapi sudah dapat teratasi pada saat revisi
