#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <wait.h>
#define PORT 8888

char *loc = "/home/hasna/shift3/hartakarun";

void send_file(FILE *fp, int sockfd) {
	int n;
	char data[1024] = {0};

	while(fgets(data, 1024, fp) != NULL) {
		if(send(sockfd, data, sizeof(data),0) == -1) {
			perror("Error in sending file");
			exit(-1);
		}
		bzero(data,1024);
	}
}

int main (int argc, char const *argv[]) {
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	char command[100];
	pid_t child_id;
	FILE *fp;

	scanf("%[^\n]s", command);
	char buffer[1024] = {0};
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons (PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	}
	child_id = fork();
	if(child_id < 0)
 	{
		exit(EXIT_FAILURE);
	}

	if(child_id == 0)
	{
		char *argv[] = {"zip", "-r", "/home/hasna/Client/hartakarun.zip", "/home/hasna/shift3/hartakarun", NULL};
		execv("/bin/zip", argv);
	}
	else {
	while(wait(NULL)>0);
	if(strcmp(command, "send hartakarun.zip") == 0) {
		send(sock, command, strlen(command), 0);
		fp = fopen("hartakarun.zip", "r");
		if(fp == NULL) {
			perror("Error in reading file");
			exit(-1);
		}
		send_file(fp,sock);
		printf("File data sent successfully \n");
	}
	valread = read(sock, buffer,1024);
	}
	printf("Closing the connection\n");
	close(sock);
	return 0;
}
