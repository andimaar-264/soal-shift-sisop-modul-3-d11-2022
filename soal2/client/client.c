#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <termios.h>
#include <stdbool.h>

#define PORT 8080
#define MAX_LENGTH 1024

pthread_t input, printx;

typedef struct user
{
    char input[1024];
    char file[1024];
    char mode[1024];
    int is_auth;
    int socket;
} user_data;

int error(char *err)
{
    perror(err);
    exit(EXIT_FAILURE);
}

void message(char input[])
{
    char buffer[1024];
    sprintf(buffer, "\n%s\n", input);
    send(user_data.socket, buffer, 1024, 0);
}

void *user_input(void *arg)
{
    while (strcmp(user_data.mode, "recvstrings") == 0)
    {
        bzero(buffer, MAX_LENGTH);
        fgets(buffer, MAX_LENGTH, stdin);
        buffer[strcspn(buffer, "\n")] = 0;

        send(user_data.socket, buffer, MAX_LENGTH, 0);

        char cmd_line[MAX_LENGTH];
        strcpy(cmd_line, buffer);
        char *cmd = strtok(cmd_line, " ");

        for (int i = 0; cmd[i]; i++)
        {
            cmd[i] = tolower(cmd[i]);
        }
    }
}

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    else
    {
        user_data.socket = sock;
        printf("Connected to server with the address %d\n", sock);
    }

    strcpy(user_data.mode, "recvstrings");

    pthread_create(&printx, NULL, &user_printx, (void *)&sock);
    pthread_create(&input, NULL, &user_input, (void *)&sock);

    while (1)
    {
        if (pthread_join(input, NULL) == 0)
        {
            pthread_create(&input, NULL, &user_input, (void *)&sock);
        }
    }

    if (strcmp(user_data.mode, "recvstrings") == 0)
    {
        pthread_join(printx, NULL);
    }

    else
    {
        pthread_exit(&printx);
    }
}
