#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <dirent.h>
#include <libgen.h>
#include <pthread.h>
#include <errno.h>
#include <stdbool.h>

char *loc = "/home/hasna/shift3/hartakarun";

void list_file(char *locpath);
void lowercase(char *s);
bool is_hidden(const char *name);
char *folder_name(char *name);
void *folder(void *args);
int isDirectory(const char*path) ;
static void make_dir(const char *dir);

const char *prg;

int main(int argc, char *argv[]) {

	list_file(loc);

	return 0;
}

void list_file(char *locpath)
{
	char path[1000];
	struct dirent *dp;
	struct  stat statbuf;
	pthread_t t_id[200];
	int i=0;
	DIR *dir = opendir(locpath);

	if(!dir)
		return;

	while((dp = readdir(dir)) != NULL)
	{
		if (strcmp(dp->d_name,".")!=0 && strcmp(dp->d_name, "..")!=0)
		{

			strcpy(path, locpath);
			strcat(path, "/");
			strcat(path, dp->d_name);

			list_file(path);
			if (isDirectory(path))
			{
			rmdir(path);
			}
			else {
			pthread_create(&t_id[i], NULL, &folder, (void *)path);
			pthread_join(t_id[i], NULL);
			i++;
			}
		}
	}
	closedir(dir);
}

void lowercase(char *s) {
	int i=0;
	while(s[i]!='\0') {
		if(s[i]>='A' && s[i]<='Z') {
			s[i]=s[i]+32;
		}
		i++;
	}
}

bool is_hidden(const char *name)
{
	if (name[0] == '.' && strcmp(name, ".")!=0 && strcmp(name,"..") !=0){
		return true;
	}
	else return false;
}

char *folder_name(char *name) {
	struct stat st;
	char *dot = strrchr(name, '.');
	if (is_hidden(name)) return "Hidden";
	if(!dot || dot == name) return "Unknown";
	else return dot+1;
}

void *folder(void *args) {
	char newpath[1000];
	char *path = (char*)malloc(sizeof(char));
	path = (char*)args;
	char *name = basename(path);
	char *ext =strdup(folder_name(name));
	
	lowercase(ext);
	if(strcmp(ext,"gz")== 0) {
		ext = "tar.gz";
	}
	strcpy(newpath, loc);
	strcat(newpath, "/");
	strcat(newpath, ext);
	strcat(newpath, "/");
	make_dir(newpath);
	strcat(newpath, name);
	rename(path, newpath);
}
int isDirectory(const char*path) {
	struct stat statbuf;
	if(stat(path, &statbuf) != 0)
		return 0;
	return S_ISDIR(statbuf.st_mode);
}

static void make_dir(const char *dir) {
	if(mkdir(dir,0755) < 0) {
		if (errno != EEXIST) {
			perror(dir);
			exit(1);
		}
	}
}
