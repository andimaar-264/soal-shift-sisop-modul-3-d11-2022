#include<stdio.h>
#include<sys/socket.h>
#include<stdlib.h>
#include<netinet/in.h>
#include<string.h>
#include<unistd.h>
#include<pthread.h>
#include<stdbool.h>
#define PORT 8080
#define serv "/home/aaliyah/sisop/modul3-soal2/Server/"

pthread_t input, received;

typedef struct user {
	char name[1024];
	char pwd[1024];
	char file[1024];
	char mode[1024];
    int is_auth;
    int socket;
} user_data;

int error(char *err){
    perror(err);
    exit(EXIT_FAILURE);
} // error

void message(char input[]){
    char buffer[1024];
    sprintf(buffer, "\n%s\n", input);
    send(user_data.socket, buffer, 1024, 0);
} // print


void setfile(char cmd[])
{ 
    char *fname;
    cmd = strtok(NULL, " "); 
    fname = cmd;
    memset(user_data.file, 0, 1000);
    strcpy(user_data.file, fname);
} // set name


void regist(char id[], char password[]){
    FILE *fp = fopen("user.txt", "a"); 
    fprintf(fp, "%s:%s\n", id, password);
    fclose(fp);
} // reg


int login(char id[], char password[])
{
    FILE *fp = fopen("user.txt", "r");
    int is_auth = 0;
    char buffer[1024];
    while (fgets(buffer, 1024, fp) != NULL && is_auth == 0)
    {
        char file_id[1024], file_password[1024];
        char *token = strtok(buffer, ":");
        strcpy(file_id, token);
        token = strtok(NULL, "\n");
        strcpy(file_password, token);

        if (strcmp(id, file_id) == 0 && strcmp(password, file_password) == 0)
        {
            is_auth = 1;
        }
        else
        {
            is_auth = 0;
        }
    }
    fclose(fp);
    return is_auth;
} // login


int addtsv(){
    FILE *fp;
    fp = fopen("problems.tsv", "a+"); 
    if (fp == NULL) // doesn't exist ~> make file
    {
        printf("File cannot be opened");
        return 1;
    }
    fprintf(fp, "\n%s\t%s", user_data.file, user_data.name);
    fclose(fp);
    return 0;
} // adding to tsv

void makedir(char title[]){
    int check;
    char *dirname = title;

    check = mkdir(dirname, 0777);

    if (!check){
        receive_file(user_data.socket, user_data.file);
    }
    else(
        message("Folder is already on the server\n");
        strcpy(user_data.mode, "recvstrings");
    )
}

int receive_file(int socket, char *fname)
{
    char buffer[MAX_LENGTH] = {0};
    char fpath[MAX_LENGTH];

    strcpy(fpath, serv);
    strcat(fpath, fname);

    char msg[1024], dpath[1024], ipath[1024], opath[1024];

    sprintf(dpath, "%s/description.txt", fpath);
    sprintf(ipath, "%s/input.txt", fpath);
    sprintf(opath, "%s/output.txt", fpath);

    

    sprintf(msg, "Filepath description.txt: %s\n
                Filepath input.txt: %s\n
                Filepath output.txt: %s\n", dpath, ipath, opath);
    message(msg);

    FILE* file_ptr = fopen(dpath, "w");
        fclose(file_ptr);

    FILE* file_ptr = fopen(ipath, "w");
        fclose(file_ptr);

    FILE* file_ptr = fopen(opath, "w");
        fclose(file_ptr);

    addtsv();                         
    strcpy(user_data.mode, "recvstrings");
}

void loginsuccess()
{
    char msg[1024], buffer[1024], title[1024];
    message("\e[1;1H\e[2J");
    printf("\nUser %s Has successfully logged in.\n", user_data.name);
    message("Login Success!");
    while (strcmp(buffer, "exit") != 0 || strcmp(user_data.mode, "recvstrings") == 0)
    {
        message("Input Command: ");
        read(user_data.socket, buffer, 1024);
        char cmd_line[MAX_LENGTH];
        strcpy(cmd_line, buffer);
        char *cmd = strtok(cmd_line, " ");
        for (int i = 0; cmd[i]; i++)
        {
            cmd[i] = tolower(cmd[i]);
        }
        if (strcmp(cmd, "add") == 0)
        {
            message("Judul Problems: ");
            read(user_data.socket, title, 1024);

            memset(user_data.file, 0, 1000);
            strcpy(user_data.file, title);

            makedir(title); 
        }
        else if (strcmp(cmd, "see") == 0)
        { 
            //seeallfile();
        }
        else if (strcmp(cmd, "download") == 0)
        { 
            // setfile(cmd);
            // download(user_data.socket, user_data.file);
        }
        // submit
        char *cmdcpy, *hasil;
        cmdcpy = cmdt;
        hasil = strstr(cmdcpy, "submit");
        else if(hasil)
        {
		    submitfile(...);
        }
        else
        {
            printf("\nArgument Invalid!");
            message("Argument Invalid!");
        }
    }
}

void *input_main(){
    char buffer[1024];
    while (1){
        if (user_data.is_auth == 0){ 
            message("1. Login\n" "2. Register\n" "Choices: ");

            read(user_data.socket, buffer, 1024); 
            for (int i = 0; buffer[i]; i++)
            { 
                buffer[i] = tolower(buffer[i]);
            }

            // login
            if (strcmp(buffer, "login") == 0 || strcmp(buffer, "1") == 0)
            {
                char id[1024];
                char password[1024];
                message("\e[1;1H\e[2J");
                message("Username: ");
                read(user_data.socket, id, 1024);

                message("Password: ");
                read(user_data.socket, password, 1024);

                user_data.is_auth = login(id, password); 
                if (user_data.is_auth == 0){ 
                    message("\e[1;1H\e[2J");
                    message("Login failed id/password is wrong!");
                    printf("Login failed id/password is wrong!\n");
                    break;
                }
                else if (user_data.is_auth == 1){
                    strcpy(user_data.name, id);
                    strcpy(user_data.pwd, password);
                    strcpy(user_data.mode, "recvstrings");
                    loginsuccess();
                }
            }
            // register
            if (strcmp(buffer, "register") == 0 || strcmp(buffer, "2") == 0)
            {
                char id[1024];
                char password[1024];
                int num = low = up = 0;
                message("\e[1;1H\e[2J");
                message("Username: ");
                read(user_data.socket, id, 1024);

                message("Password: ");
                read(user_data.socket, password, 1024);

                if(strlen(password) >=  6){
			        for(k = 0; k < strlen(ps); k++){
				        if(isdigit(password[k]) != 0) num++;
				        if(ps[k] >= 'a' && <= 'z') low++;
				        if(ps[k] >= 'A' && <= 'Z') up++;
			        }

			        if(num > 0 && low > 0 && up > 0){
				        regist(id, password); 

                        char msg[1024];
                        message("\e[1;1H\e[2J");
                        printf("User %s is registered", id);
                        sprintf(msg, "User %s Register Completed... (Press Anything to Continue)", id);
                        message(msg);
			        }
		        }
                else if (strlen(password) <  6){
                    message("\e[1;1H\e[2J");
                    message("Password must be at least 6 characters and must contain numeric, lowercase, and uppercase letter");
                    printf("Password must be at least 6 characters and must contain numeric, lowercase, and uppercase letter\n");
                }
                break;
            }
        }

        else if (user_data.is_auth == 1){ 
            loginsuccess();
        }
    }
}

int main(int argc, char const *argv[]){
    int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);
	char buffer[1024] = {0};

	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        	perror("socket failed");
	        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        	perror("setsockopt");
	        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0){
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 1) < 0){
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((clientsocket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0){
        perror("accept");
        exit(EXIT_FAILURE);
    }

    else{
        printf("Connected with client with the address: %d\n", clientsocket);
        user_data.socket = clientsocket;
    }
    user_data.is_auth = 0;

    pthread_create(&input, NULL, &input_main, 0);
    while (1){ 
        if (pthread_join(input, NULL) == 0){
            pthread_create(&input, NULL, &input_main, 0);
        }
    }
}
